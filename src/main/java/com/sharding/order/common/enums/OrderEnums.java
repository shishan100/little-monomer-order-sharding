package com.sharding.order.common.enums;

/**
 * @author ruyuan
 * 订单枚举类
 */
public enum OrderEnums {
    PENDING_PAYMENT(10, "待付款"),
    PENDING_ORDER(20, "待接单"),
    RECEIVED_ORDER(30, "已接单"),
    IN_DELIVERY(40, "配送中"),
    COMPLETED(50, "已完成"),
    PARTIAL_REFUND(55, "部分退款"),
    FULL_REFUND(60, "全部退款"),
    CANCEL_ORDER(70, "取消订单")
    ;
    public final Integer code;
    public final String desc;

    OrderEnums(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
