package com.sharding.order.common.enums;

/**
 * 订单操作类型
 */
public enum OrderOperateType {
    add(10,"新增"),
    updateOrderStatus(20,"修改订单状态");

    public final Integer code;
    public final String desc;

    OrderOperateType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
