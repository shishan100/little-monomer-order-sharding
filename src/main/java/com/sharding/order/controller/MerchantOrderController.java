package com.sharding.order.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sharding.order.bean.web.DataResponse;
import com.sharding.order.bean.web.OperationResponse;
import com.sharding.order.bean.web.PageResponse;
import com.sharding.order.common.enums.OrderCode;
import com.sharding.order.domain.query.MerchantOrderInfoQuery;
import com.sharding.order.domain.vo.MerchantOrderVO;
import com.sharding.order.domain.vo.OrderDetailVO;
import com.sharding.order.exception.BaseException;
import com.sharding.order.service.merchant.MerchantOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ruyuan
 * @Description
 */
@Slf4j
@RestController
@RequestMapping(value = "/merchant/order")
public class MerchantOrderController {

    @Autowired
    private MerchantOrderService merchantOrderService;

    /**
     * 查询商户订单列表
     * @param merchantOrderInfoQuery
     * @return
     */
    @GetMapping("/queryOrderInfoByMerchant")
    public PageResponse queryOrderInfoByMerchant(@RequestBody MerchantOrderInfoQuery merchantOrderInfoQuery){
        //开始计时
        long bTime = System.currentTimeMillis();
        try{
            Page<MerchantOrderVO> merchantOrderVOPage = merchantOrderService.queryOrderInfoByMerchant(merchantOrderInfoQuery);
            //关闭分段计时
            long eTime = System.currentTimeMillis();
            //输出
            log.info("查询用户订单耗时：[{}]",(eTime-bTime));
            return PageResponse.success(merchantOrderVOPage);
        } catch (Exception e){
            return PageResponse.error(OrderCode.QUERY_ORDER_ERROR.getDesc());
        }
    }

    /**
     * 接单。
     * @param orderNo
     * @return
     */
    @PostMapping("/receiveOrder")
    public OperationResponse receiveOrder(@RequestParam("orderNo")String orderNo){
        try{
            //开始计时
            long bTime = System.currentTimeMillis();
            merchantOrderService.receiveOrder(orderNo);
            //关闭分段计时
            long eTime = System.currentTimeMillis();
            //输出
            log.info("商家处理订单耗时：[{}]",(eTime-bTime));
            return OperationResponse.success(OrderCode.UPDATE_ORDER_SUCCESS.desc);
        }catch (Exception e){
            return OperationResponse.error(OrderCode.UPDATE_ORDER_ERROR.getDesc());
        }
    }

    @GetMapping("getMerchantOrderDetail")
    public DataResponse getMerchantOrderDetail(@RequestParam("orderNo")String orderNo){
        try{
            //开始计时
            long bTime = System.currentTimeMillis();
            OrderDetailVO orderDetailVOList = merchantOrderService.getOrderDetail(orderNo);
            //关闭分段计时
            long eTime = System.currentTimeMillis();
            //输出
            log.info("查询商户订单明细耗时：[{}]",(eTime-bTime));
            return DataResponse.success(orderDetailVOList);
        }catch (BaseException e){
            return DataResponse.error(e.getMessage());
        }catch (Exception e){
            return DataResponse.error(OrderCode.QUERY_ORDER_ERROR.getDesc());
        }

    }
}
