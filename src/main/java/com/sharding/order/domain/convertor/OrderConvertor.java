package com.sharding.order.domain.convertor;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sharding.order.bean.request.order.UserOrderInfoRequest;
import com.sharding.order.domain.entity.OrderInfo;
import com.sharding.order.domain.query.MerchantOrderInfoQuery;
import com.sharding.order.domain.query.OrderInfoBaseQuery;
import com.sharding.order.domain.query.UserOrderInfoQuery;
import com.sharding.order.domain.vo.MerchantOrderVO;
import com.sharding.order.domain.vo.OrderInfoVO;
import com.sharding.order.domain.vo.UserOrderVO;
import org.mapstruct.Mapper;

/**
 * @author ruyuan
 */
@Mapper(componentModel = "spring")
public interface OrderConvertor {
    /**
     *用户订单request转实体
     * @param userOrderInfoRequest
     * @return
     */
    OrderInfo dtoConvertOrderInfo (UserOrderInfoRequest userOrderInfoRequest);

    /**
     * 商户订单查询转订单基础查询
     * @param merchantOrderInfoQuery
     * @return
     */
    OrderInfoBaseQuery merchantQueryToBaseOrder (MerchantOrderInfoQuery merchantOrderInfoQuery);

    /**
     *
     * @param orderInfoVOPage
     * @return
     */
    Page<MerchantOrderVO> OrderVoToMerchantInfoVo(Page<OrderInfoVO> orderInfoVOPage);
    /**
     * 用户订单查询转订单基础查询
     * @param userOrderInfoQuery
     * @return
     */
    OrderInfoBaseQuery userQueryToBaseOrder (UserOrderInfoQuery userOrderInfoQuery);

    /**
     *
     * @param orderInfoVOPage
     * @return
     */
    Page<UserOrderVO> OrderVoToUserInfoVo(Page<OrderInfoVO> orderInfoVOPage);
}
