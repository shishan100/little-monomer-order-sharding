package com.sharding.order.domain.valid;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.sharding.order.bean.request.order.UserOrderInfoRequest;
import com.sharding.order.domain.query.UserOrderInfoQuery;
import com.sharding.order.exception.BaseException;
import org.springframework.util.CollectionUtils;

/**
 * @author ruyuan
 * 订单入参校验类
 */
public class OrderValidation {

    /**
     * 检查校验订单查询参数
     * @param userOrderInfoQuery
     */
    public static void checkVerifyOrderQuery(UserOrderInfoQuery userOrderInfoQuery) {
        if (ObjectUtils.isEmpty(userOrderInfoQuery)) {
            throw new BaseException("入参对象不能为空");
        }

        if (ObjectUtils.isEmpty(userOrderInfoQuery.getPageSize())) {
            throw new BaseException("参数：条数不能为空");
        }

        if (ObjectUtils.isEmpty(userOrderInfoQuery.getPageNo())) {
            throw new BaseException("参数：当前页不能为空");
        }

        if (ObjectUtils.isEmpty(userOrderInfoQuery.getUserId())) {
            throw new BaseException("参数：用户id不能为空");
        }
    }

    public static void checkVerifyOrderRequest(UserOrderInfoRequest userOrderInfoRequest){
        if (ObjectUtils.isEmpty(userOrderInfoRequest)) {
            throw new BaseException("参数：入参对象不能为空");
        }
        if (CollectionUtils.isEmpty(userOrderInfoRequest.getOrderItemDetailList())){
            throw new BaseException("订单明细不能为空");
        }
        if (ObjectUtils.isEmpty(userOrderInfoRequest.getMerchantId())){
            throw new BaseException("订单商户不能为空");
        }
        if (ObjectUtils.isEmpty(userOrderInfoRequest.getAddressId())){
            throw new BaseException("收货地址不能为空");
        }
        if (ObjectUtils.isEmpty(userOrderInfoRequest.getOrderAmount())){
            throw new BaseException("订单金额不能为空");
        }
        if (ObjectUtils.isEmpty(userOrderInfoRequest.getPayAmount())){
            throw new BaseException("订单支付金额不能为空");
        }
    }

}
