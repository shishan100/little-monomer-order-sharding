package com.sharding.order.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sharding.order.domain.entity.OrderInfo;
import com.sharding.order.domain.query.OrderInfoBaseQuery;
import com.sharding.order.domain.vo.OrderInfoVO;
import com.sharding.order.domain.vo.OrderDetailVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author ruyuan
 * 用户订单mapper
 */
@Mapper
public interface UserOrderInfoMapper {

    /**
     * 插入订单
     *
     * @param orderInfo 入参
     * @return 出参
     */
    int insertSelective(@Param("orderInfo") OrderInfo orderInfo);

    /**
     * 用户取消订单
     *
     * @param orderNo
     * @param status
     */
    void cancelOrder(@Param("orderNo") String orderNo, @Param("status") Integer status);

    /**
     * 根据条件分页查询订单列表
     *
     * @param page               入参
     * @param orderInfoBaseQuery 入参
     * @return 出参
     */
    Page<OrderInfoVO> queryOrderInfoList(Page<OrderInfoVO> page, @Param("record") OrderInfoBaseQuery orderInfoBaseQuery);

    /**
     * 根据订单号查询订单信息
     *
     * @param orderNo
     * @return
     */
    int getOrderStatus(@Param("orderNo") String orderNo);

    /**
     * 根据订单号查订单信息
     *
     * @param orderNo
     * @return
     */
    OrderDetailVO getOrderInfoByNo(@Param("orderNo") String orderNo);

    /**
     * 修改订单状态
     *
     * @param orderNo
     * @param status
     */
    void updateStatus(@Param("orderNo") String orderNo, @Param("status") Integer status);
}