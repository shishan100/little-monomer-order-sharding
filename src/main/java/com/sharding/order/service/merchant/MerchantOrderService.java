package com.sharding.order.service.merchant;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sharding.order.domain.query.MerchantOrderInfoQuery;
import com.sharding.order.domain.vo.MerchantOrderVO;
import com.sharding.order.domain.vo.OrderDetailVO;

/**
 * @author ruyuan
 * 商户订单管理service
 */
public interface MerchantOrderService {

    /**
     * 获取订单列表
     * @param merchantOrderInfoQuery
     * @return
     */
    Page<MerchantOrderVO> queryOrderInfoByMerchant(MerchantOrderInfoQuery merchantOrderInfoQuery);

    /**
     * 根据订单号获得订单详情
     * @param orderNo
     * @return
     */
    OrderDetailVO getOrderDetail(String orderNo);

    /**
     * 商家接单/拒单
     * @param orderNo
     */
    void receiveOrder(String orderNo);







}
