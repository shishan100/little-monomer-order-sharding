package com.sharding.order.service.user;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sharding.order.bean.request.order.UserOrderInfoRequest;
import com.sharding.order.domain.query.UserOrderInfoQuery;
import com.sharding.order.domain.vo.OrderDetailVO;
import com.sharding.order.domain.vo.UserOrderVO;

/**
 * @author ruyuan
 * 用户订单service
 */
public interface UserOrderInfoService {
    /**
     * 用户提交订单
     *
     * @param userOrderInfoRequest
     */
    void generateOrder(UserOrderInfoRequest userOrderInfoRequest);

    /**
     * 根据查询条件获得订单列表
     *
     * @param userOrderInfoQuery
     * @return
     */
    Page<UserOrderVO> queryUserOrderInfoList(UserOrderInfoQuery userOrderInfoQuery);

    /**
     * 根据订单号查询订单详情
     *
     * @param orderNo
     * @return
     */
    OrderDetailVO getOrderDetail(String orderNo);

    /**
     * 用户取消订单
     *
     * @param orderNo
     */
    void cancelOrder(String orderNo);


    /**
     * 根据redis取消订单
     * @param orderNo
     */
    void redisCancelOrder(String orderNo);

}
